<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Login Route(s)
 */
Route::get('loginadmin', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('loginadmin', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
/**
 * Password Reset Route(S)
 */
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
/**
 * Email Verification Route(s)
 */
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');


Route::get('/', 'WelcomeController@index')->name('index');

Route::get('/posts/index', 'PostsController@index')->name('posts/index');
Route::resource('posts', 'PostsController');

Route::get('/jobs/index', 'JobsController@index')->name('jobs/index');
Route::resource('jobs', 'JobsController');

//CREATE JOBS
Route::get('/jobs/create', 'JobsController@create')->name('jobs.create');
Route::post('/jobs/store', 'JobsController@store')->name('jobs.store');

//USERS
Route::get('/users/index', "UsersController@index")->name('users/index');
Route::resource('users', 'UsersController');
