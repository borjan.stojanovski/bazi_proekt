<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //Table name
    protected $table = 'Borjan.posts';
    //Primary key
    public $primaryKey = 'post_id';

    public $timestamps = True;

    public function users()
    {
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    }
}
