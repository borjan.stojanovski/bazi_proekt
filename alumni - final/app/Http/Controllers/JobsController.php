<?php

namespace App\Http\Controllers;

use App\Company;
use App\Job;
use Illuminate\Http\Request;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $jobs = Job::all();
        return view('jobs.index', compact('jobs'));
    }

    public function applied()
    {
        //
        $jobs = Job::all();
        return view('jobs.applied', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $companies = Company::all();
        $jobs = Job::all();
        return view('jobs.create', compact('companies', 'jobs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate(array(
            'job_id'=>'required',
            'job_title' => 'required',
            'job_description' => 'required',
            'job_offer_end_date' => 'required',
            'company_id' => 'required|numeric',
        ));

        $job = new Job();
        $job->job_id = $request->get('job_id');
        $job->job_title = $request->get('job_title');
        $job->job_description = $request->get('job_description');
        $job->job_offer_end_date = $request->get('job_offer_end_date');
        $job->company_id = $request->get('company_id');
        $job->save();
//        dd($job);

        return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $job = Job::find($id);
        return view('jobs.show')->with('job', $job);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $job = Job::find($id);
        $companies = Company::all();
        return view('jobs.edit', compact('job', 'companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedData = $request->validate(array(
            'job_id'=>'required',
            'job_title' => 'required',
            'job_description' => 'required',
            'job_offer_end_date' => 'required',
            'company_id' => 'required|numeric',
        ));

        $job = Job::find($id);
        $job->job_title = $request->get('job_title');
        $job->job_description = $request->get('job_description');
        $job->job_offer_end_date = $request->get('job_offer_end_date');
        $job->company_id = $request->get('company_id');
        dd($job);

        $job->save();

        return redirect()->route('jobs/index')->with('success', 'Update successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
