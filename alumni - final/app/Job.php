<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    //Table name
    protected $table = 'Borjan.jobs';
    //Primary key
    public $primaryKey = 'job_id';

    public $timestamps = false;

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'company_id');
    }
}
