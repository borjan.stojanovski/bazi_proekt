<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employed extends Model
{
    //
    protected $primaryKey = 'contract_id';

    public function users()
    {
        return $this->belongsTo(Users::class, 'user_id', 'user_id');
    }
}
