<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $table = 'Borjan.companies';
    protected $primaryKey = 'company_id';
    public $timestamps = True;

    /**
     * Get the comments for the blog post.
     */
    public function job()
    {
        return $this->hasMany( Job::class, 'company_id', 'company_id');
    }

}
