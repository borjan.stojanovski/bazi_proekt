<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //
    protected $table = 'Borjan.users';
    protected $primaryKey = 'user_id';
    public $timestamps = True;

    /**
     * Get the comments for the blog post.
     */
    public function post()
    {
        return $this->hasMany( Post::class, 'user_id', 'user_id');
    }

}
