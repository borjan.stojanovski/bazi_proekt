@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="/users/index" class="btn" style="font-size: 24px; color: black"><u>Users</u></a>

        <div class="row">

            @foreach($users as $user)
                <div class="col-sm-3 col-xs-12">
                    <div class="panel panel-default text-center">
                        <div class="panel-heading" style="background-color: #5f90a8 !important;">
                            <h4>{{$user->user_name}} {{$user->lastname}}</h4>
                        </div>
                        <div class="panel-body">
                        </div>
                        <div class="panel-footer">
                            <h6>{{$user->mail}}</h6>
                            <h4>{{$user->birth_year}}</h4>
                            <a href="/users/{{$user->user_id}}" class="btn btn-lg" style="background-color: #5f90a8;">View More</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>


@endsection
