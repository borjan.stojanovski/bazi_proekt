@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row"  style="margin-bottom: 2%">
            <div class="col-md-1">
                    <h4 class="col-md-4">Searching: </h4>
            </div>
            <div class="col-md-4" style="margin-left: 2%;">
                <input type="text" name="search" id="search-input" class="form-control search col-6" placeholder="Search Deal Data" aria-label="Username" aria-describedby="basic-addon1">
            </div>
        </div>
    </div>


    @if(count($posts) >= 0)
        <div class="section">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- post -->
                    @foreach($posts as $post)
                        <a href="/posts/{{$post->post_id}}">
                        <div class="col-md-4">
                            <div class="card-deck">
                            <div class="card cards">
                                <img src="https://cdn3.vectorstock.com/i/1000x1000/62/57/hud-elements-search-human-resources-hr-vector-20386257.jpg" height="100%" width="100%" alt="">
                                <div class="card-body">
                                    <h4 class="card-title">{{$post->post_title}}</h4>
                                    <p class="card-text">{{$post->users->user_name}} {{$post->users -> lastname}}</p>
                                    <p class="card-text"><small class="text-muted">{{$post->post_date}}</small></p>
                                </div>
                            </div>
                            </div>
                        </div>
                        </a>
                @endforeach
                <!-- /post -->
                </div>
                <!-- /row -->
            </div>
        </div>
    @else
        <p>No Posts Found</p>
    @endif

    <script>
        $(document).ready(function() {
            $("#search-input").on('keyup', function() {
                var value = $(this).val().toLowerCase();

                $('.cards').filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>

@endsection
