@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <img src="https://cdn3.vectorstock.com/i/1000x1000/62/57/hud-elements-search-human-resources-hr-vector-20386257.jpg" height="100%" width="60%" alt="">
                <h1>{{$post->post_title}}</h1>
                <h4>{{$post->users->user_name}} {{$post->users->lastname}}  <span style="font-size: 11px">{{$post->post_date}}</span>   </h4>
                <h3>{{$post->post_description}}</h3>
            </div>
        </div>
    </div>

@endsection
