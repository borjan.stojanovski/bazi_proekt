@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default text-center">
                        <div class="panel-heading">
                            <h1><u>{{$job->company->company_name}}</u></h1>
                            <h3>{{$job->job_title}}</h3>
                        </div>
                        <div class="panel-body text-left">
                            <p ><b>Company description:</b> {{$job->company->company_description}}</p>
                            <br><p ><b>Job description: </b>{{$job->job_description}}
                                <br>
                                {{$job->job_description}}
                                <br>
                                It usually begins with: “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.” The purpose of lorem ipsum is to create
                                a natural looking block of text (sentence, paragraph, page, etc.) that doesn't distract from the layout...</p>
                            <br>
                            <p><b>Qualifications: </b></p>
                                <ul>
                                    <li>Proven working experience as a full-stack web developer.</li>
                                    <li>Top-notch programming skills, and in-depth knowledge of modern JavaScript, TypeScript, HTML, and CSS.</li>
                                    <li>In-depth knowledge of at least one major framework, like React, Vue or Angular 2.</li>
                                    <li>Enterprising problem diagnosis and creative problem-solving skills.</li>
                                    <li>HTML, CSS, Bootstrap, JavaScript, Angular, Java, Python, asp.Net</li>
                                </ul><br>
                            <p><b>Level: </b> Entry Level</p>
                        </div>

                        <div class="panel-footer">
                            <a href="/jobs/{{$job->job_id}}" class="btn btn-lg">Apply</a>
                            <h4 style="color: #1a1a1a">Position is open till: {{$job->job_offer_end_date}}</h4>
                        </div>
                    </div>

                    <div class="text-right">
                        <a href="/jobs/{{$job->job_id}}/edit" class="btn btn-warning">EDIT</a>
                    </div>
                </div>
        </div>
    </div>

@endsection
