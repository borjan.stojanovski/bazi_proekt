@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="sidenav">
                    <div class="card"><br>
                        <article class="card-group-item">
                            <div class="filter-content">
                                <div class="card-body">
                                    <div class="form-row">
                                        <h4 class="text-center" style="color:white">Salary: </h4>
                                        <div class="form-group col-md-6">
                                            <input type="text" placeholder="$0" class="form-control" style="width: 60px">
                                        </div>
                                        <div class="form-group col-md-6 text-right" >
                                            <input type="text" placeholder="$1,00" class="form-control" style="width: 60px">
                                        </div>
                                    </div>
                                </div> <!-- card-body.// -->
                            </div>
                        </article> <!-- card-group-item.// -->

                        <article class="card-group-item" style="margin-left: 4%">
                            <div class="filter-content">
                                <div class="card-body">
                                    <br><br><br><br>
                                    <h4 class="text-center padingBIten" style="color:white; padding-top: 15px;">Технологии: </h4>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="Check1">
                                        <label class="custom-control-label" for="Check1">C#</label>
                                    </div> <!-- form-check.// -->

                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="Check2">
                                        <label class="custom-control-label" for="Check2">Java</label>
                                    </div> <!-- form-check.// -->

                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="Check3">
                                        <label class="custom-control-label" for="Check3">PHP</label>
                                    </div> <!-- form-check.// -->

                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="Check4">
                                        <label class="custom-control-label" for="Check4">Python</label>
                                    </div> <!-- form-check.// -->

                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="Check5">
                                        <label class="custom-control-label" for="Check5">NodeJS</label>
                                    </div> <!-- form-check.// -->
                                </div> <!-- card-body.// --><br><br><br>
                                <a style="font-size: 14px;" href="/jobs/create">Have you got some job? <span style="color: #fff989;"><br>click me!</span></a>
                            </div>
                        </article> <!-- card-group-item.// -->
                    </div> <!-- card.// -->
                </div>
            </div>


            <div class="col-md-10">
                @foreach($jobs as $job)
                    <div class="col-sm-4 col-xs-12">
                        <div class="panel panel-default text-center">
                            <div class="panel-heading">
                                <h2><u>{{$job->company->company_name}}</u></h2>
                                <h5>{{$job->job_title}}</h5>
                            </div>
                            <div class="panel-body">
                                <p>It usually begins with: “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.” The purpose of lorem ipsum is to create
                                    a natural looking block of text (sentence, paragraph, page, etc.) that doesn't distract from the layout...</p>
                            </div>
                            <div class="panel-footer">
                                <h4>{{$job->job_offer_end_date}}</h4>
                                <a href="/jobs/{{$job->job_id}}" class="btn btn-lg">View More</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
