@extends('layouts.app')

@section('content')

    <div class="container-fluid header pt-5">
        <div class="row">

            <div class="container">
                <div>
                    <a href="/jobs/index" class="text-muted"><i class="fas fa-arrow-left"></i></a>
                </div>
                <div class="mb-5 pt-4">
                    <h1>Внесување на работна позиција.</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container ">
        <div class="row">
            <div class="col-md-8 pt-4">
                @if($errors)
                    <div>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- FORM -->
                <form method="POST" action="{{ route('jobs.store')}}" enctype="multipart/form-data">
                @csrf



                    <!-- Број na ponudata -->
                    <div class="form-group row">
                        <label for="job_id">Број на понудата.</label>
                        <input type="number" id='name' class='form-control shadow-sm' placeholder="пр: 1" name="job_id" required>
                    </div>
                    <!--End Број na ponudata -->

                    <!-- Ime na kompanijata -->
                    <div class="form-group row">
                        <label for="job_title">Име на работата.</label>
                        <input type="text" id='job_title' class='form-control shadow-sm' placeholder="Junior Angular" name="job_title" required>
                    </div>
                    <!-- End Ime na kompanijata -->


{{--                    SELECT DROPDOWN PROBA--}}
                    @if($companies != null)
                    <div class="form-group">
                        <label for="company_id">Select Company: </label>

                        <select name="company_id" class="form-control">
                            @foreach($companies as $company)
                                <option value="{{$company->company_id}}">{{$company->company_name}}</option>
                            @endforeach
                        </select>

                    </div>
                    @endif
{{--                    ENDSELECT DROPDOWN PROBA--}}


                    <!-- Opis na ponudata -->
                    <div class="form-group row">
                        <label for="job_description">Опис на понудата.</label>
                        <textarea type="text" name="job_description" id="job_description" rows="4" cols="50" class="form-control shadow-sm " require></textarea>
                    </div>
                    <!--End Opis na ponudata -->



                    <!-- Date Offer -->
                    <div class="form-group row">
                        <label for="job_offer_end_date">Birthday:</label>
                        <input type="date" id="birthday" name="job_offer_end_date">
                    </div>
                    <!-- End Date Offer -->


                    <!-- Register Button -->
                    <div class="form-group row mb-0">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-lg btn-dark btn-dark-buy-now mb-5">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                    <!-- End Register Button -->

                </form>
            </div>
        </div>
    </div>

@endsection
