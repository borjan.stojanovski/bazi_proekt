@extends('layouts.app')

@section('content')

        {{--POSTS--}}
        @if(count($posts) >= 0)
            <div class="section">
                <div class="container">
                    <a href="/posts/index" class="btn" style="font-size: 24px; color: black"><u>Posts</u></a>
                    <!-- row -->
                    <div class="row">
                        <!-- post -->
                        @foreach($posts as $post)
                            <a href="/posts/{{$post->post_id}}">
                                <div class="col-md-6">
                                    <div class="card-deck">
                                        <div class="card">
                                            <img src="https://cdn3.vectorstock.com/i/1000x1000/62/57/hud-elements-search-human-resources-hr-vector-20386257.jpg" height="100%" width="100%" alt="">
                                            <div class="card-body">
                                                <h4 class="card-title">{{$post->post_title}}</h4>
                                                <p class="card-text">{{$post->users->user_name}} {{$post->users -> lastname}}</p>
                                                <p class="card-text"><small class="text-muted">{{$post->post_date}}</small></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                    @endforeach
                    <!-- /post -->
                    </div>
                    <!-- /row -->
                </div>
            </div>
        @endif
        {{--END POSTS--}}
        <hr>




        {{--JOBS--}}
        <div class="container">
            <a href="/jobs/index" class="btn" style="font-size: 24px; color: black"><u>Jobs</u></a>

            <div class="row">

                @foreach($jobs as $job)
                <div class="col-sm-4 col-xs-12">
                    <div class="panel panel-default text-center">
                        <div class="panel-heading">
                            <h2><u>{{$job->company->company_name}}</u></h2>
                            <h5>{{$job->job_title}}</h5>
                        </div>
                        <div class="panel-body">
                            <p>It usually begins with: “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.” The purpose of lorem ipsum is to create
                                a natural looking block of text (sentence, paragraph, page, etc.) that doesn't distract from the layout...</p>
                        </div>
                        <div class="panel-footer">
                            <h4>{{$job->job_offer_end_date}}</h4>
                            <a href="/jobs/{{$job->job_id}}" class="btn btn-lg">View More</a>
                        </div>
                    </div>
                </div>
                    @endforeach
            </div>
        </div>

        {{--END JOBS--}}



        <div class="container">
            <a href="/users/index" class="btn" style="font-size: 24px; color: black"><u>Users</u></a>

            <div class="row">

                @foreach($users as $user)
                    <div class="col-sm-3 col-xs-12">
                        <div class="panel panel-default text-center">
                            <div class="panel-heading" style="background-color: #5f90a8 !important;">
                                <h4>{{$user->user_name}} {{$user->lastname}}</h4>
                            </div>
                            <div class="panel-body">
                            </div>
                            <div class="panel-footer">
                                <h6>{{$user->mail}}</h6>
                                <h4>{{$user->birth_year}}</h4>
                                <a href="/jobs/{{$user->user_id}}" class="btn btn-lg" style="background-color: #5f90a8;">View More</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>


@endsection
